#!/bin/ruby

require 'gitlab'

puts '-- Forker --'
puts 'This script will fork a list of websites into a given folder'
puts 'It needs the following things:'
puts "\t1: A list of student websites in students.txt"
puts "\t2: an ID for the location where each repo should be cloned"
puts "\t3: A private_token in a .env file from someone who has permission to fork into that folder"

MY_TARGET = 5665728

if ENV['TOKEN']
  the_token = ENV['TOKEN']
elsif File.file?('.env')
  the_token = File.open('.env').read.strip
end

if the_token.nil?
  puts 'Missing token!'
  return
end

Gitlab.endpoint = 'https://gitlab.com/api/v4'
# user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
Gitlab.private_token = the_token

# Read student list
File.open('students.txt').each do |i|
  # Get username and project name from list
  username = i.split('/')[2].chomp('.gitlab.io')
  website = i.strip.split('/').last
  p '-----'
  p "#{username} - #{website}"

  # Find user_id with username
  user_id = Gitlab.users({ username: username }).first.id
  p "User id: #{user_id}"

  # Find correct project in the user's projects
  project_id = Gitlab.user_projects(user_id, { search: website }).first.id
  p "Project id: #{project_id}"

  # Create fork project in the new location MY_TARGET
  new_project = Gitlab.create_fork(project_id, { namespace_id: MY_TARGET })

  # Run one pipeline in order to deploy the new website.
  Gitlab.create_pipeline(new_project.id, 'master')
end
