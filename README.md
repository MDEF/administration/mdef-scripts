
To run Gitlab scripts you need a personal access token

Create one here:

https://gitlab.com/profile/personal_access_tokens


## Setup

1. Install the Gitlab gem with

`bundler install` or `gem install gitlab`

2. Run a script with:

`ruby forker.rb`
